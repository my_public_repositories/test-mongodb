# test-mongodb



## Getting started

launch docker compose file
import data with:

```mongoimport --db new_york --authenticationDatabase admin --username root --password root --collection restaurants /data/to_import/restaurants.json```

and then:

```
mongosh "mongodb://root:root@localhost:27017/"
use new_york
db.getCollection("restaurants").find({})
db.getCollection("restaurants").find({"borough":"Brooklyn",'cuisine':"Italian","address.street":"5 Avenue"}).count()
db.getCollection("restaurants").find({"borough":"Brooklyn",'cuisine':"Italian","address.street":"5 Avenue", "name" : /pizza/i},{"name":1,"grades.score":1}) 
```

have fun !