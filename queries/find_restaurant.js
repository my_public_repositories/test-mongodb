const { MongoClient } = require('mongodb');

async function main() {
    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/drivers/node/ for more details
     */
    const uri = "mongodb://root:root@localhost:27017/";
    
    /**
     * The Mongo Client you will use to interact with your database
     * See https://mongodb.github.io/node-mongodb-native/3.6/api/MongoClient.html for more details
     * In case: '[MONGODB DRIVER] Warning: Current Server Discovery and Monitoring engine is deprecated...'
     * pass option { useUnifiedTopology: true } to the MongoClient constructor.
     * const client =  new MongoClient(uri, {useUnifiedTopology: true})
     */
    const client = new MongoClient(uri);

    try {
        // Connect to the MongoDB cluster
        await client.connect();    
        // Establish and verify connection
        db = client.db("new_york");
        await db.command({ ping: 1 });
        console.log("Connected successfully to server");
        // Make the appropriate DB calls
        let restaurants =db.collection("restaurants")
        
        let fifth_av_italian__dinners_count = await restaurants.countDocuments({"borough":"Brooklyn",'cuisine':"Italian","address.street":"5 Avenue"})
        console.log(fifth_av_italian__dinners_count + " italian dinners on the 5 avenue")

        let my_dinners = await restaurants.find({"borough":"Brooklyn",'cuisine':"Italian","address.street":"5 Avenue", "name" : /pizza/i},{"name":1,"grades.score":1}) 
        console.log(my_dinners)

        db.getCollection('restaurants').find(
            {"borough":"Manhattan",
             "grades.score":{$lt : 10}
            },
            {"name":1,"grades.score":1, "_id":0})
        

        
    } finally {
        // Close the connection to the MongoDB cluster
        await client.close();
    }
}

main().catch(console.error);


